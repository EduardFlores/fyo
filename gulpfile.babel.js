/* Importaciones */

import gulp from "gulp";
/* ***** sass ***** */
import sass from "gulp-sass";
/* ***** cache bust ***** */
import cachebust from "gulp-cache-bust";
/* ***** optimizar imagenes ***** */
import imagemin from "gulp-imagemin";
/* ***** browser sync ***** */
import { init as server, stream, reload } from "browser-sync";
/* ***** plumber ***** */
import plumber from "gulp-plumber";
/* ***** autoprefixer ***** */
import autoprefixer from "gulp-autoprefixer";

/* +++++++++++++++++++++++++++++++++++++++++ */
/* Tareas */

/* ***** Tarea Sass ***** */
gulp.task("sass", () => {
  return gulp
    .src("./src/scss/main.scss")
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "expanded",
      })
    )
    .pipe(gulp.dest("./public/css"))
    .pipe(stream());
});
/* ***** Tarea cache-bust ***** */
gulp
  .src("./public/*.html")
  .pipe(
    cachebust({
      type: "timestamp",
    })
  )
  .pipe(gulp.dest("./public"));

/* ***** Tarea de imagemin ***** */
/* ejecutar esto solo una vez  */
gulp.task("imgmin", () => {
  return gulp
    .src("./src/images/**/*")
    .pipe(plumber())
    .pipe(
      imagemin([
        imagemin.mozjpeg({ quality: 30, progressive: true }),
        imagemin.optipng({ optimizationLevel: 1 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
        }),
      ])
    )
    .pipe(gulp.dest("./public/assets/images"));
});
// vigilante
gulp.task("default", () => {
  server({
    server: "./public",
  });
  gulp.watch("./public/*.html").on("change", reload);
  gulp.watch("./src/scss/**/*.scss", gulp.series("sass"));
});
